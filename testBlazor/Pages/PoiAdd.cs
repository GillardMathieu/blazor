﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Threading.Tasks;
using testBlazor.Models;

namespace testBlazor.Pages
{
    public partial class PoiAdd
    {
        public PointOfInterest selectedPoi = new PointOfInterest();
        public bool isEditionMode;
        public string editionModeString = "Mode Edition";
        public List<PoiCategory> PoiCats;
        private int _selectedId;

        protected override async Task OnInitializedAsync()
        {
            try
            {
                var res = await client.GetAsync("category");
                if (res.IsSuccessStatusCode)
                {
                    string json = await res.Content.ReadAsStringAsync();
                    PoiCats = Newtonsoft.Json.JsonConvert.DeserializeObject<PoiCategory[]>(json).ToList<PoiCategory>();
                    isEditionMode = false;

                    StateHasChanged();
                }


            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }

        public async void SendPoi()
        {
            try
            {
                selectedPoi.Category_id = _selectedId;
                var res = await client.PostAsJsonAsync<PointOfInterest>("pointinteret/", selectedPoi);

                if(res.IsSuccessStatusCode)
                {
                    await JSRuntime.InvokeVoidAsync("toastr.success", "Ajout réussi");
                    if (isEditionMode == true)
                    {
                        editionModeString = "Mode Edition";
                        isEditionMode = false;
                        await JSRuntime.InvokeAsync<List<string>>("myDestroyFunction");
                    }

                    NavManager.NavigateTo("/Poi");
                }
                else
                {
                    await JSRuntime.InvokeVoidAsync("toastr.error", "Ajout non effectué");
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }

        public async void EditionMode()
        {
            if (isEditionMode == false)
            {
                editionModeString = "Retour Normal";
                isEditionMode = true;
                await JSRuntime.InvokeVoidAsync("myFunction");
            }
            else
            {
                editionModeString = "Mode Edition";
                isEditionMode = false;
                List<string> val = (await JSRuntime.InvokeAsync<List<string>>("myDestroyFunction"));
                selectedPoi.Description_fr = val[0];
                selectedPoi.Description_nl = val[1];
                selectedPoi.Description_en = val[2];
            }
        }
    }
}
