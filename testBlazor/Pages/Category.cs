﻿using Blazored.Toast.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Threading.Tasks;
using testBlazor.Models;
using Microsoft.JSInterop;

namespace testBlazor.Pages
{
    public enum Color
    {
        black = 0,
        blue = 1,
        gold = 2,
        green = 3,
        grey = 4,
        orange = 5,
        violet = 6,
        yellow = 7
    }
    public partial class Category
    {
        public string _nameFr;
        public string _nameNl;
        public string _nameEn;

        public Color poiColor = new Color();
        public Color categoryPoiColor = new Color();

        public PoiCategory _sendedCategory = new PoiCategory();
        public PoiCategory _selectedCategory = new PoiCategory();

        public List<PoiCategory> PoiCats;
        private int _selectedId;

        protected override async Task OnInitializedAsync()
        {

            try
            {
                var res = await client.GetAsync("category");
                if (res.IsSuccessStatusCode)
                {
                    string json = await res.Content.ReadAsStringAsync();
                    PoiCats = Newtonsoft.Json.JsonConvert.DeserializeObject<PoiCategory[]>(json).ToList<PoiCategory>();
                }


            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }

        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            JSRuntime.InvokeVoidAsync("select2", "mySelect");
            return base.OnAfterRenderAsync(firstRender);
        }

        public async void CategoryAdd()
        {
            try
            {
                _sendedCategory.Name_fr = _nameFr;
                _sendedCategory.Name_nl = _nameNl;
                _sendedCategory.Name_en = _nameEn;
                _sendedCategory.PinColor = poiColor.ToString();

                var res = await client.PostAsJsonAsync("category", _sendedCategory);

                if (res.IsSuccessStatusCode)
                {
                    await JSRuntime.InvokeVoidAsync("toastr.success", "Ajout réussi");

                    int i = int.Parse(await res.Content.ReadAsStringAsync());

                    _sendedCategory.Id = i;

                    PoiCats.Add(_sendedCategory);

                    _nameFr = null;
                    _nameNl = null;
                    _nameEn = null;

                    StateHasChanged();
                }
                else
                {
                    await JSRuntime.InvokeVoidAsync("toastr.error", "Ajout non effectué");
                }


            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }

        }


        public void GetDescription(int id)
        {
            if (id > 0)
            {
                try
                {
                    _selectedCategory = PoiCats.FirstOrDefault(x => x.Id == id);
                    _selectedId = _selectedCategory.Id;

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    throw;
                }
            }
        }

        public async void HardDelete(int id)
        {
            if (id > 0)
            {
                try
                {
                    var res = await client.DeleteAsync("category/" + id);

                    if (res.IsSuccessStatusCode)
                    {
                        await JSRuntime.InvokeVoidAsync("toastr.success", "Suppression réussie");

                        PoiCategory p = PoiCats.FirstOrDefault(x => x.Id == id);

                        PoiCats.Remove(p);

                        StateHasChanged();

                        _selectedId = 0;
                        _selectedCategory = new PoiCategory();

                        StateHasChanged();
                    }
                    else
                    {
                        await JSRuntime.InvokeVoidAsync("toastr.error", "Suppression echouée");
                    }

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    throw;
                }
            }
        }
        public async void CategoryUpdate(int id)
        {
            try
            {
                var res = await client.PutAsJsonAsync("category", _selectedCategory);

                if (res.IsSuccessStatusCode)
                {
                    await JSRuntime.InvokeVoidAsync("toastr.success", "Mise à jour réussie");

                    _selectedId = 0;
                    _selectedCategory = new PoiCategory();

                    StateHasChanged();
                }
                else
                {
                    await JSRuntime.InvokeVoidAsync("toastr.error", "Mise à jour echouée");
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }

        public async void SoftDelete()
        {
            try
            {
                _selectedCategory.IsDeleted = !_selectedCategory.IsDeleted;
                
                var res = await client.PutAsJsonAsync("category", _selectedCategory);

                if (res.IsSuccessStatusCode)
                {
                    await JSRuntime.InvokeVoidAsync("toastr.success", "Mise à jour réussie");
                    StateHasChanged();
                }
                else
                {
                    await JSRuntime.InvokeVoidAsync("toastr.error", "Mise à jour echouée");
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }
        public void Dispose()
        {
            JSRuntime.InvokeVoidAsync("select2Destroy");
        }

    }
}
