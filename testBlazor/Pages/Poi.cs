﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Threading.Tasks;
using testBlazor.Models;

namespace testBlazor.Pages
{
    public partial class Poi
    {
        private List<PointOfInterest> maListePoi;
        public PointOfInterest selectedPoi = new PointOfInterest();
        public bool isEditionMode;
        public string editionModeString = "Mode Edition";

        public List<PoiCategory> PoiCats;
        private int _selectedId;
        private int _selectedPoiId;

        protected override async Task OnInitializedAsync()
        {
            try
            {
                var res = await HttpClient.GetAsync("pointinteret");
                if (res.IsSuccessStatusCode)
                {
                    string json = await res.Content.ReadAsStringAsync();
                    maListePoi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PointOfInterest>>(json).ToList<PointOfInterest>();
                    isEditionMode = false;
                }
                var res2 = await HttpClient.GetAsync("category");
                if (res2.IsSuccessStatusCode)
                {
                    string json2 = await res2.Content.ReadAsStringAsync();
                    PoiCats = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PoiCategory>>(json2).ToList<PoiCategory>();
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }

        public void getDescription(int id)
        {
            if (id > 0)
            {
                if (isEditionMode == true)
                {
                    editionModeString = "Mode Edition";
                    isEditionMode = false;
                    JSRuntime.InvokeAsync<List<string>>("myDestroyFunction");
                }

                try
                {
                    selectedPoi = maListePoi.FirstOrDefault(x => x.Id == id);
                    _selectedId = selectedPoi.Category_id;

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    throw;
                }
            }
        }

        public async void UpdatePoi()
        {
            try
            {
                selectedPoi.Category_id = _selectedId;
                var res = await HttpClient.PutAsJsonAsync<PointOfInterest>("pointinteret/", selectedPoi);

                if(res.IsSuccessStatusCode)
                {
                    await JSRuntime.InvokeVoidAsync("toastr.success", "Mise à jour réussi");
                }
                else
                {
                    await JSRuntime.InvokeVoidAsync("toastr.error", "Mise à jour échouée");
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }

        public async void SoftDelete()
        {
            try
            {
                selectedPoi.IsDeleted = !selectedPoi.IsDeleted;

                var res = await HttpClient.PutAsJsonAsync<PointOfInterest>("pointinteret/", selectedPoi);

                if (res.IsSuccessStatusCode)
                {
                    await JSRuntime.InvokeVoidAsync("toastr.success", "Mise à jour réussi");
                    StateHasChanged();
                }
                else
                {
                    await JSRuntime.InvokeVoidAsync("toastr.error", "Mise à jour échouée");
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }

        public async void DeletePoi()
        {
            try
            {
                var res = await HttpClient.DeleteAsync("pointinteret/" + selectedPoi.Id);

                if (res.IsSuccessStatusCode)
                {
                    await JSRuntime.InvokeVoidAsync("toastr.success", "Suppression réussie");
                    maListePoi.Remove(selectedPoi);

                    StateHasChanged();

                    _selectedPoiId = 0;
                    selectedPoi = new PointOfInterest();
                    _selectedId = 0;

                    StateHasChanged();
                }
                else
                {
                    await JSRuntime.InvokeVoidAsync("toastr.error", "Suppression échouée");
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
        }

        public async void EditionMode()
        {
            if (isEditionMode == false)
            {
                editionModeString = "Retour Normal";
                isEditionMode = true;
                await JSRuntime.InvokeVoidAsync("myFunction");
            }
            else
            {
                editionModeString = "Mode Edition";
                isEditionMode = false;
                List<string> val = (await JSRuntime.InvokeAsync<List<string>>("myDestroyFunction"));
                selectedPoi.Description_fr = val[0];
                selectedPoi.Description_nl = val[1];
                selectedPoi.Description_en = val[2];
            }
        }
    }
}
